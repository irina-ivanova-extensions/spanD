## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
The extension calculates how many days and hours are between two datetimes; or calculates the datetime in specified days and hours.

# Download and Install
* **[Install](https://chrome.google.com/webstore/detail/span-of-datetimes/eifmojfjlkacjfnaeagfnfjgpajanphn "Install")** last version from Google Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](irina.ivanova@protonmail.com) or create an issue here.

# Description
Extension is build for software testers to calculate spans or differences between two datetimes.

Useful tool for software testers or developers who need to test span between datetimes. For example, if software should automatically calculate next datetime in specified amount of dates.

Extension can be used completely without a mouse! Enter key calculates the span or the next date.

First publication of the extension - November 18, 2013.

# Useful To Know
* Format for date input is European DD.MM.YYYY (days first, not months)
* Regexp for inserting datetimes: `/([0,1,2,3]?[0-9])[\., \,, \-, \/]?([0,1,2][0-9])[\., \,, \-, \/]?([1,2][0-9][0-9][0-9])\s([0,1,2][0-9])[:, \., \,, \-]?([0,1,2,3,4,5][0-9])/`
   So `31.02.2010`, `31022010`, `31/02/2010`, `31-02-2010` will work.
* Works with negative values! For example, add `-1` days to `02.10.2013` will be `01.10.2013`. Or span between `4.10.2010` and `1.10.2010` will be `-3`.

# Hot Keys
* `[Enter]` makes calculations
* `[Up Arrow]` in the bottom block field sets focus on the upper block
* `[Up Arrow]` in the upper section sets focus on the first line
* `[Down Arrow]` in the upper block field sets focus on the bottom block
* `[Down Arrow]` in the bottom section sets focus on the second line
* `[Left Arrow]` sets focus on the left field (if there is one)
* `[Right Arrow]` sets focus on the right field (if there is one)
* `[Tab]` sets focus in further order:
  * Start Date
  * End Date
  * Date
  * Days
  * Hours
  * Minutes

# Chrome Tip
You can configure hot-keys for extension in the Google Chrome:
* open the extensions tab - `chrome://extensions`
* link "Configure commands" at the bottom
* choose an extension and type a shortcut
* now You can use it completely without a mouse!

# Posts About spanD
* *January 29, 2017* [Profile Page with HTML5 and CSS3](https://ivanova-irina.blogspot.nl/2017/01/profile-page-with-html5-and-css3.html "Profile Page with HTML5 and CSS3")
* *April 12, 2014* [spanD v1.1](http://ivanova-irina.blogspot.com/2014/04/spand-v11.html "spanD v1.1")
* *March 3, 2014* [spanD v1.0](http://ivanova-irina.blogspot.com/2014/03/spand-v10.html "spanD v1.0")
* *January 3, 2014* [Span Of Datetimes v0.4](http://ivanova-irina.blogspot.com/2014/01/span-of-datetimes-v04.html "Span Of Datetimes v0.4")
* *December 3, 2013* [Span Of Datetimes v0.3](http://ivanova-irina.blogspot.com/2013/12/span-of-datetimes-03.html "Span Of Datetimes v0.3")
* *November 20, 2013* [Span Of Datetimes v0.2](http://ivanova-irina.blogspot.com/2013/11/span-of-datetimes-v02.html "Span Of Datetimes v0.2")
* *November 18, 2013* [Span Of Datetimes v0.1](http://ivanova-irina.blogspot.com/2013/11/span-of-datetimes-01.html "Span Of Datetimes v0.1")
