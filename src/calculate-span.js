function calculateSpan() {
	clearSpanResult();
	clearError();

	setSpanVariables();

	var days=0;
	var hours=0;
	var minutes=0;

	days = Math.floor((end.getTime() - start.getTime()) / oneDay);
	hours = Math.floor((end.getTime() - start.getTime() - days*oneDay) / oneHour);
	minutes = Math.floor((end.getTime() - start.getTime() - days*oneDay - hours*oneHour) / oneMinute);

	returnSpanResult(days, hours, minutes);
}
