if(window.addEventListener) {
    window.addEventListener("load", listenKeys, false);
    window.addEventListener("focus", listenFocus, false);
  } else if(window.attachEvent) {
    window.attachEvent("onload", listenKeys);
    window.addEventListener("focus", listenFocus);
  } else {
    document.addEventListener("load", listenKeys, false);
    document.addEventListener("focus", listenFocus, false);
  }

function listenFocus(){
	listenStartDateFocus(document.getElementById("start"));
	listenEndDateFocus(document.getElementById("end"));

	listenAdditionDateFocus(document.getElementById("date"));
	listenAddDaysFocus(document.getElementById("days"));
	listenAddHoursFocus(document.getElementById("hours"));
  listenAddMinutesFocus(document.getElementById("minutes"));

}

function listenStartDateFocus(inputString) {
	if(inputString.addEventListener)
		inputString.addEventListener("focus", setShadowOnCalculateAddition, false);
    else if(inputString.attachEvent)
    	inputString.attachEvent("focus", setShadowOnCalculateAddition);
}

function listenEndDateFocus(inputString) {
  if(inputString.addEventListener)
    inputString.addEventListener("focus", setShadowOnCalculateAddition, false);
    else if(inputString.attachEvent)
      inputString.attachEvent("focus", setShadowOnCalculateAddition);
}

function listenAdditionDateFocus(inputString) {
  if(inputString.addEventListener)
    inputString.addEventListener("focus", setShadowOnCalculateSpan, false);
    else if(inputString.attachEvent)
      inputString.attachEvent("focus", setShadowOnCalculateSpan);
}

function listenAddDaysFocus(inputString) {
  if(inputString.addEventListener)
    inputString.addEventListener("focus", setShadowOnCalculateSpan, false);
    else if(inputString.attachEvent)
      inputString.attachEvent("focus", setShadowOnCalculateSpan);
}

function listenAddHoursFocus(inputString) {
  if(inputString.addEventListener)
    inputString.addEventListener("focus", setShadowOnCalculateSpan, false);
    else if(inputString.attachEvent)
      inputString.attachEvent("focus", setShadowOnCalculateSpan);
}

function listenAddMinutesFocus(inputString) {
  if(inputString.addEventListener)
    inputString.addEventListener("focus", setShadowOnCalculateSpan, false);
    else if(inputString.attachEvent)
      inputString.attachEvent("focus", setShadowOnCalculateSpan);
}

function setShadowOnCalculateSpan() {
	enableShadow("span");
	disableShadow("addition");
}

function setShadowOnCalculateAddition() {
	enableShadow("addition");
	disableShadow("span");
}

function listenKeys() {

    listenStartDate(document.getElementById("start"));
    listenEndDate(document.getElementById("end"));

	listenDate(document.getElementById("date"));
	listenDays(document.getElementById("days"));
	listenHours(document.getElementById("hours"));
	listenMinutes(document.getElementById("minutes"));
}

var enter=13;
var leftArrow=37;
var upArrow=38;
var rightArrow=39;
var downArrow=40;

function listenStartDate(inputStartDate) {
	if (inputStartDate.addEventListener)
        inputStartDate.addEventListener("keydown", startDateListener, false);
    else if (inputStartDate.attachEvent)
        inputStartDate.attachEvent("keydown", startDateListener);
}

function startDateListener(e) {
	if (e.keyCode === enter)
		calculateSpan();

	if (e.keyCode === downArrow)
		setFocusOnAdditionDate();

	if (e.keyCode === rightArrow)
		setFocusOnStartTime();
}

function listenEndDate(inputEndDate) {
	if (inputEndDate.addEventListener)
        inputEndDate.addEventListener("keydown", endDateListener, false);
    else if (inputEndDate.attachEvent)
        inputEndDate.attachEvent("keydown", endDateListener);
}

function endDateListener(e) {
	if (e.keyCode === enter)
		calculateSpan();

	if (e.keyCode === downArrow)
		setFocusOnAdditionDate();

	if (e.keyCode === upArrow)
		setFocusOnStartDate();

	if (e.keyCode === rightArrow)
		setFocusOnEndTime();
}

function listenDate(inputDate) {
	if (inputDate.addEventListener)
        inputDate.addEventListener("keydown", dateListener, false);
    else if (inputDate.attachEvent)
        inputDate.attachEvent("keydown", dateListener);
}

function dateListener(e) {
	if (e.keyCode === enter)
		calculateAddition();

	if (e.keyCode === upArrow)
		setFocusOnStartDate();

	if (e.keyCode === rightArrow)
		setFocusOnTime();

	if (e.keyCode === downArrow)
		setFocusOnDays();
}

function listenDays(inputDays) {
	if (inputDays.addEventListener)
        inputDays.addEventListener("keydown", daysListener, false);
    else if (inputDays.attachEvent)
        inputDays.attachEvent("keydown", daysListener);
}

function daysListener(e) {
	if (e.keyCode === enter)
		calculateAddition();

	if (e.keyCode === upArrow)
		setFocusOnStartDate();

	if (e.keyCode === rightArrow)
		setFocusOnHours();
}

function listenHours(inputHours) {
	if (inputHours.addEventListener)
        inputHours.addEventListener("keydown", hoursListener, false);
    else if (inputHours.attachEvent)
        inputHours.attachEvent("keydown", hoursListener);
}

function hoursListener(e) {
	if (e.keyCode === enter)
		calculateAddition();

	if (e.keyCode === upArrow)
		setFocusOnStartDate();

	if (e.keyCode === rightArrow)
		setFocusOnMinutes();

	if (e.keyCode === leftArrow)
		setFocusOnDays();
}

function listenMinutes(inputMinutes) {
	if (inputMinutes.addEventListener)
        inputMinutes.addEventListener("keydown", minutesListener, false);
    else if (inputMinutes.attachEvent)
        inputMinutes.attachEvent("keydown", minutesListener);
}

function minutesListener(e) {
	if (e.keyCode === enter)
		calculateAddition();

	if (e.keyCode === upArrow)
		setFocusOnStartDate();

	if (e.keyCode === leftArrow)
		setFocusOnHours();
}
