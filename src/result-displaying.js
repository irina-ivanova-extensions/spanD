function returnError(errorText) {
	setErrorText(errorText);
	throw "";
}

function setErrorText(errorText) {
	var divError = document.getElementById("error");
	divError.innerText = errorText + "\n";
}

function returnSpanResult(days, hours, minutes) {
	var divResult = document.getElementById("result");
	divResult.innerText = ">>> span:\u00A0" + days + "d "
										+ hours + "h "
										+ minutes + "m \n";
}

function returnAdditionResult(date) {
	var month = setMonth(date.getMonth()+1);
	var hour = setHour(date.getHours());
	var minute = setMinute(date.getMinutes());

	setAdditionResultText(date, month, hour, minute);
	selectCalculatedAddition();
}

function setMonth(month) {
	if (isMonthShort(month))
		return "0" + month.toString();
	else
		return month;
}

function isMonthShort(month) {
	if (month.toString().length === 1)
		return true;
	else
		return false;
}

function setHour(hour) {
	if (isHourShort(hour))
		return "0" + hour.toString();
	else
		return hour;
}

function isHourShort(hour) {
	if (hour.toString().length === 1)
		return true;
	else
		return false;
}

function setMinute(minute) {
	if (isMinuteShort(minute))
		return "0" + minute.toString();
	else
		return minute;
}

function isMinuteShort(minute) {
	if (minute.toString().length === 1)
		return true;
	else
		return false;
}

function setAdditionResultText(date, month, hour, minute) {
	var calculatedAddition = document.getElementById("add-result");
	calculatedAddition.value = date.getDate() + "."
								+ month + "."
								+ date.getFullYear() + " "
								+ hour + ":"
								+ minute;
}

function clearError() {
	var clearError = document.getElementById("error");
	clearError.innerText = "" + "\n";

	clearAllRedBorders();
}

function clearAllRedBorders() {
	var allElements = document.getElementsByTagName("*");

	for (var i = 0; i < allElements.length; i++) {
		clearRedBorder(allElements[i]);
	}
}

function clearRedBorder(element) {
	element.style.border = "";
}

function clearSpanResult() {
	var clearResult = document.getElementById("result");
	clearResult.innerText = ">>> span: -d -h -m" + "\u00A0\n";
}

function setFocusOnStartDate() {
	document.getElementById("start").focus();
}

function setFocusOnEndDate() {
	document.getElementById("end").focus();
}

function setFocusOnAdditionDate() {
	document.getElementById("date").focus();
}

function setFocusOnDays() {
	document.getElementById("days").focus();
}

function setFocusOnHours() {
	document.getElementById("hours").focus();
}

function setFocusOnMinutes() {
	document.getElementById("minutes").focus();
}

function selectCalculatedAddition() {
	document.getElementById("add-result").select();
}

function enableShadow(divId) {
	document.getElementById(divId).style.opacity = 0.4;
}

function disableShadow(divId) {
	document.getElementById(divId).style.opacity = 1;
}
