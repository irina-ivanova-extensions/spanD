var start;
var end;
var additionDate;
var addDays;
var addHours;
var addMinutes;

var dateRegexp = /([0,1,2,3]?[0-9])[\., \,, \-, \/]?([0,1,2][0-9])[\., \,, \-, \/]?([1,2][0-9][0-9][0-9])\s([0,1,2][0-9])[:, \., \,, \-]?([0,1,2,3,4,5][0-9])/;
var daysAndHoursRegexp = /[\-]?[0-9]+/;

var oneMinute = 60 * 1000; // second * millisecond
var oneHour = 60 * oneMinute;
var oneDay = 24 * oneHour;

function setSpanVariables() {

	var inputStartDate = document.getElementById("start").value;
	var inputEndDate = document.getElementById("end").value;

	checkMandatorySpanFields(inputStartDate, inputEndDate);

	setTime("start", inputStartDate);
	setTime("end", inputEndDate);

	setDatetime("start", document.getElementById("start").value);
	setDatetime("end", document.getElementById("end").value);
}

function checkMandatorySpanFields(inputStartDate, inputEndDate) {
	if (inputStartDate === "" && inputEndDate === "")
		returnError("Insert dates");
	else if (inputStartDate === "")
		returnError("Insert start date");
	else if (inputEndDate === "")
		returnError("Insert end date");
}

function setTime(id, date) {
	if (date.length <= 10)
		setNullTime(id, date);
}

function setNullTime(id, value) {
	document.getElementById(id).value = value + " " + "00:00";
}

function setDatetime(globalVarName, date) {
	var dateArray = dateRegexp.exec(date);

	checkDateFormat(globalVarName, dateArray);

	if (globalVarName === "start")
		start = new Date(
			(+dateArray[3]),
			(+dateArray[2])-1,
			(+dateArray[1]),
			(+dateArray[4]),
			(+dateArray[5]));
	else if (globalVarName === "end")
		end = new Date(
				(+dateArray[3]),
				(+dateArray[2])-1,
				(+dateArray[1]),
				(+dateArray[4]),
				(+dateArray[5]));
	else if (globalVarName === "date")
		additionDate = new Date(
		    	(+dateArray[3]),
		    	(+dateArray[2])-1,
		    	(+dateArray[1]),
		    	(+dateArray[4]),
		    	(+dateArray[5]));
}

function checkDateFormat(type, date) {
	if (type === "start" && date === null)
		returnError("Invalid start datetime format");
	else if (type === "end" && date === null)
		returnError("Invalid end datetime format");
	else if (type === "date" && date === null)
		returnError("Invalid datetime format");
}

function setAdditionVariables() {

	var inputDate = document.getElementById("date").value;
	var inputDays = document.getElementById("days").value;
	var inputHours = document.getElementById("hours").value;
	var inputMinutes = document.getElementById("minutes").value;

	checkMandatoryAdditionFileds(inputDate);

	if (inputDate.length <= 10)
		setNullTime("date", inputDate);

	setNullDays(inputDays);
	setNullHours(inputHours);
	setNullMinutes(inputMinutes);

	checkDaysFormat(document.getElementById("days").value);
	checkHoursFormat(document.getElementById("hours").value);
	checkMinutesFormat(document.getElementById("minutes").value);

	setDatetime("date", document.getElementById("date").value);
}

function checkMandatoryAdditionFileds(inputDate) {
	if (inputDate === "")
		returnError("Insert datetime");
}

function setNullDays(inputDays) {
	if (inputDays === "") {
		document.getElementById("days").value = "0";
		addDays = "0";
	} else
		addDays = inputDays;
}

function checkDaysFormat(inputDays) {
	if (daysAndHoursRegexp.exec(inputDays) === null || daysAndHoursRegexp.exec(inputDays)[0] != inputDays)
		returnError("Invalid days format");
}

function setNullHours(inputHours) {
	if (inputHours === "") {
		document.getElementById("hours").value = "0";
		addHours = "0";
	} else
		addHours = inputHours;
}

function checkHoursFormat(inputHours) {
	if (daysAndHoursRegexp.exec(inputHours) === null || daysAndHoursRegexp.exec(inputHours)[0] != inputHours)
		returnError("Invalid hours format");
}

function setNullMinutes(inputMinutes) {
	if (inputMinutes === "") {
		document.getElementById("minutes").value = "0";
		addMinutes = "0";
	} else
		addMinutes = inputMinutes;
}

function checkMinutesFormat(inputMinutes) {
	if (daysAndHoursRegexp.exec(inputMinutes) === null || daysAndHoursRegexp.exec(inputMinutes)[0] != inputMinutes)
		returnError("Invalid minutes format");
}
