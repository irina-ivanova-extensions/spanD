function calculateAddition() {

	clearError();

	setAdditionVariables();

	var newDate = new Date();

	newDate.setTime(additionDate.getTime() + (addDays * oneDay) + (addHours * oneHour) + (addMinutes * oneMinute));

	returnAdditionResult(newDate);
}